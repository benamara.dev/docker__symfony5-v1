# Install image php: https://hub.docker.com/_/php
FROM php:7.4-fpm-alpine
 
# Apk install + Install Git
RUN apk --no-cache update && apk --no-cache add bash git
 
# Install pdo
RUN docker-php-ext-install pdo_mysql
 
# Install composer + Execute composer + Remove composer + Move composer in path: https://getcomposer.org/doc/00-intro.md https://getcomposer.org/download/
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" && php composer-setup.php && php -r "unlink('composer-setup.php');" && mv composer.phar /usr/local/bin/composer
 
# Install Symfony CLI https://symfony.com/download + Install it globally
RUN wget https://get.symfony.com/cli/installer -O - | bash && mv /root/.symfony/bin/symfony /usr/local/bin/symfony
# bash n'est pas intégrer dans alpine par defaut: Installer Apk (voir en haut)

# Ouvrir le container dans le dossier html
WORKDIR /var/www/html
